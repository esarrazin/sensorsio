#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright: (c) 2024 CESBIO / Centre National d'Etudes Spatiales
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
""" 
Module for reading ERA5 and ERA5-land
"""

import sys
import zipfile
from dataclasses import dataclass
from enum import Enum
from typing import Any, List, Optional, Tuple, cast

import numpy as np
import psutil  # type: ignore
import rasterio as rio
import xarray as xr
from pyproj import CRS
from rasterio.warp import reproject

from sensorsio.utils import bb_transform


@dataclass
class ERA5DataInfo:
    """Class for describing ERA5 data"""
    key: str
    label: str
    unit: str
    description: str


class ERA5Var(ERA5DataInfo, Enum):
    """ 
    ERA5 variables
    """
    DEWPOINT_TEMPATURE = ("d2m", "2m dewpoint temperature", "K",
            "This parameter is the temperature to which the air, "\
            "at 2 metres above the surface of the Earth, "\
            "would have to be cooled for saturation to occur.")
    TEMPERATURE = ("t2m", "2m temperature", "K",
            "This parameter is the temperature of air at 2m "\
            "above the surface of land, sea or inland waters.")
    SURFACE_PRESSURE = ("sp", "Surface pressure", "Pa",
            "This parameter is the pressure (force per unit area) "\
            "of the atmosphere at the surface of land, sea and inland water. ")
    SURFACE_SOLAR_RADIATION_DOWNWARD_CLEAR_SKY = ("ssrdc",
            "Surface solar radiation downward, clear sky",    "J m-2",
            "This parameter is the amount of solar radiation "\
            "(also known as shortwave radiation) that reaches "
            "a horizontal plane at the surface of the Earth, "
            "assuming clear-sky (cloudless) conditions. "\
            "This parameter is accumulated over a particular time "\
            "period which depends on the data extracted. "\
            "For the reanalysis, the accumulation period is "\
            "over the 1 hour ending at the validity date and time.")
    SURFACE_SOLAR_RADIATION_DOWNWARD = ("ssrd",
            "Surface solar radiation downwards", "J m-2",
            "This parameter is the amount of solar radiation "\
            "(also known as shortwave radiation) that reaches a horizontal "\
            "plane at the surface of the Earth. "\
            "This parameter is accumulated over a particular "\
            "time period which depends on the data extracted. "\
            "For the reanalysis, the accumulation period is over "\
            "the 1 hour ending at the validity date and time.")
    SURFACE_THERMAL_RADIATION_DOWNWARD_CLEAR_SKY = ("strdc",
            "Surface thermal radiation downward, clear sky", "J m-2",
            "This parameter is the amount of thermal (also known "\
            "as longwave or terrestrial) radiation emitted by "\
            "the atmosphere that reaches a horizontal plane at the "\
            "surface of the Earth, "\
            "assuming clear-sky (cloudless) conditions. "\
            "For the reanalysis, the accumulation period is "\
            "over the 1 hour ending at the validity date and time.")
    SURFACE_THERMAL_RADIATION_DOWNWARD = ("strd",
            "Surface thermal radiation downwards", "J m-2",
            "This parameter is the amount of thermal (also "\
            "known as longwave or terrestrial) radiation emitted "\
            "by the atmosphere and clouds that reaches a horizontal "\
            "plane at the surface of the Earth. "\
            "This parameter is accumulated over a particular time "\
            "period which depends on the data extracted. "\
            "For the reanalysis, the accumulation period is over "\
            "the 1 hour ending at the validity date and time.")
    TOTAL_COLUMN_OZONE = ("tco3", "Total column ozone", "kg m-2",
            "This parameter is the total amount of ozone in a column "\
            "of air extending from the surface of the Earth "\
            "to the top of the atmosphere.")
    TOTAL_COLUMN_WATER = ("tcw", "Total column water", "kg m-2",
            "This parameter is the sum of water vapour, liquid water, "\
            "cloud ice, rain and snow in a column extending from "\
            "the surface of the Earth to the top of the atmosphere.")
    TOTAL_COLUMN_WATER_VAPOR = ("tcwv", "Total column water vapour",
            "kg m-2", "This parameter is the total amount of water "\
            "vapour in a column extending from the surface of "\
            "the Earth to the top of the atmosphere.")
    TOTAL_PRECIPITATION = ("tp", "Total precipitation", "m",
            "This parameter is the accumulated liquid and frozen water, "\
            "comprising rain and snow, that falls to the Earth's surface."
            "This parameter is accumulated over a particular time "\
            "period which depends on the data extracted. "\
            "For the reanalysis, the accumulation period is over "\
            "the 1 hour ending at the validity date and time.")
    U_WIND = ("u10", "10m u-component of wind", "m s-1",
            "This parameter is the eastward component of "\
            "the 10m wind. It is the horizontal speed of air "\
            "moving towards the east, at a height of ten metres "\
            "above the surface of the Earth, in metres per second.")
    V_WIND = ("v10", "10m v-component of wind", "m s-1",
            "This parameter is the northward component of the "\
            "10m wind. It is the horizontal speed of air moving "\
            "towards the north, at a height of ten metres above "\
            "the surface of the Earth, in metres per second.")

    @classmethod
    def from_key(cls, key):
        """
        Create enum from a key value
        """
        for value in cls:
            if value.key == key:
                return cls((value.key, value.label, value.unit, value.description))
        raise ValueError(f"No variable found with key {key}")

    @classmethod
    def _missing_(cls, value):
        """
        Overload the missing method to call from_key method
        if enum is instanciated with a string
        """
        if isinstance(value, str):
            return cls.from_key(value)
        return super()._missing_(value)


class ERA5Data():
    """ ERA5 data model"""
    def __init__(
        self,
        filename,
    ) -> None:
        self.filename = filename
        self.crs = CRS.from_epsg(4326).to_string()
        # Get available variables
        self.__available_variables = []
        with xr.open_dataset(zipfile.ZipFile(self.filename).open("data.nc")) if zipfile.is_zipfile(
                self.filename) else xr.open_dataset(self.filename) as ds:
            for var in ds.data_vars:
                self.__available_variables.append(ERA5Var.from_key(var))
            self.__available_dates = [
                date.strftime('%Y-%m-%d %H:%M:%S') for date in ds.indexes["time"]
            ]
            lon: np.ndarray = self._trunc(ds.longitude.data, 6)
            lat: np.ndarray = self._trunc(ds.latitude.data, 6)
            self.res = self._trunc(lon[1] - lon[0], 6)
            west, south, east, north = np.min(lon) - self.res / 2, np.min(
                lat) - self.res / 2, np.max(lon) + self.res / 2, np.max(lat) + self.res / 2
            self.transform = rio.transform.from_origin(west, north, self.res, self.res)
            self.bounds = rio.coords.BoundingBox(west, south, east, north)

    def __repr__(self):
        return f"ERA5 data file={self.filename}"

    def _trunc(self, values: np.ndarray, decs: int = 0) -> np.ndarray:
        """
        Truncate decimal digits of an array
        """
        return cast(np.ndarray, np.trunc(values * 10**decs) / (10**decs))

    def get_available_variables(self) -> List[ERA5Var]:
        """
        Return the variables avalaible in the product
        """
        return [var.key for var in self.__available_variables]

    def get_available_dates(self) -> List[str]:
        """
        Return the date avalaible in the product
        """
        return self.__available_dates

    def read_as_numpy(
        self,
        variables: Optional[List[ERA5Var]] = None,
        dates: Optional[List[str]] = None,
        crs: Optional[str] = None,
        resolution: Optional[float] = None,
        bounds: Optional[rio.coords.BoundingBox] = None,
        algorithm: rio.enums.Resampling = rio.enums.Resampling.cubic,
        dtype: np.dtype = np.dtype('float32')
    ) -> Tuple[np.ndarray[Any, Any], np.ndarray[Any, Any], np.ndarray[Any, Any], np.ndarray[
            Any, Any], str]:
        """
        Read data from ERA5 products as a numpy ndarray. 

        :param variables: The list of variables to read
        :param crs: Projection in which to read the data
        :param resolution: Resolution of data
        :param bounds: New bounds for datasets. 
        :param algorithm: The resampling algorithm to be used if needed
        :param dtype: dtype of the output Tensor
        :return: The image pixels as a np.ndarray of shape [bands, width, height],
                 The x coords as a np.ndarray of shape [width],
                 the y coords as a np.ndarray of shape [height],
                 the crs as a string

        """
        # Safeguard
        if variables is None:
            variables = self.__available_variables
        assert len(variables) > 0
        for var in variables:
            if not var.key in self.get_available_variables():
                raise ValueError(
                    f"Error variable {var.key} not found in the product ({self.get_available_variables()})"
                )

        if crs is None:
            dst_crs = self.crs
        else:
            dst_crs = crs

        # Check if we need to reproject
        need_to_reproject = False

        datasets = []

        with xr.open_dataset(zipfile.ZipFile(self.filename).open("data.nc")) if zipfile.is_zipfile(
                self.filename) else xr.open_dataset(self.filename) as ds:

            time: np.ndarray = np.array([], dtype=np.datetime64)
            if dates is not None:
                for date in dates:
                    try:
                        time = np.append(time, ds.sel(time=date).time.data)
                    except KeyError:
                        raise ValueError(f"Date {date} not found in the dataset")
            else:
                time = ds.time.data

            if self.crs == dst_crs:
                # Same CRS
                if resolution is None:
                    resolution = abs(self.transform[0])

                if bounds is not None and self.bounds != bounds:
                    # If we change bounds
                    need_to_reproject = True
                    dst_bounds = rio.coords.BoundingBox(*bounds)
                else:
                    dst_bounds = rio.coords.BoundingBox(*self.bounds)

                # If we change resolution
                if abs(self.transform[0]) != resolution:
                    need_to_reproject = True

            else:
                # Different CRS
                need_to_reproject = True

                if bounds is not None and self.bounds != bounds:
                    # If we change bounds
                    dst_bounds = rio.coords.BoundingBox(*bounds)
                else:
                    dst_bounds = bb_transform(self.crs, dst_crs, self.bounds)

                if resolution is None:
                    dst_transform = rio.transform.from_bounds(*dst_bounds, ds.sizes["longitude"],
                                                              ds.sizes["latitude"])
                    resolution = dst_transform[0]

            # If we change CRS but same resolution
            if self.crs != dst_crs and bounds is None:
                need_to_reproject = True

            left = dst_bounds.left + resolution / 2
            right = dst_bounds.right - resolution / 2
            dst_sizex = int(np.rint((right - left) / resolution)) + 1
            top = dst_bounds.top - resolution / 2
            bottom = dst_bounds.bottom + resolution / 2
            dst_sizey = int(np.rint((top - bottom) / resolution)) + 1
            dst_transform = rio.transform.from_bounds(*dst_bounds, dst_sizex, dst_sizey)

            # Safeguard to compute if there is enough memory available
            available_memory = psutil.virtual_memory().available \
                                / 1024 / 1024 / 1024 # in Gb
            requested_memory = (sys.getsizeof(dtype) * len(variables) \
                                * len(time) * dst_sizex * dst_sizey) \
                                / 1024 / 1024 / 1024
            if requested_memory > available_memory:
                raise MemoryError("Not enough memory to process the "\
                                  "dataset (requested memory = "\
                                  f"{requested_memory} / available memory "\
                                  f"= {available_memory}")
            if need_to_reproject:
                if dates is None:
                    for var in variables:
                        src_data = ds[var.key].to_numpy()
                        dst_data = np.zeros((src_data.shape[0], dst_sizey, dst_sizex), dtype)
                        reproject(src_data,
                                  dst_data,
                                  src_transform=self.transform,
                                  src_crs=self.crs,
                                  dst_transform=dst_transform,
                                  dst_crs=dst_crs,
                                  resampling=algorithm)
                        datasets.append(dst_data)
                else:
                    for var in variables:
                        sub_datasets = []
                        for date in dates:
                            src_data = ds.sel(time=date)[var.key].to_numpy()
                            if src_data.ndim == 2:
                                src_data = np.expand_dims(src_data, axis=0)
                            dst_data = np.zeros((src_data.shape[0], dst_sizey, dst_sizex), dtype)
                            reproject(src_data,
                                      dst_data,
                                      src_transform=self.transform,
                                      src_crs=self.crs,
                                      dst_transform=dst_transform,
                                      dst_crs=dst_crs,
                                      resampling=algorithm)
                            sub_datasets.append(dst_data)
                        datasets.append(np.concatenate([data for data in sub_datasets], axis=0))
            else:
                if dates is None:
                    datasets = [ds[var.key].to_numpy() for var in variables]
                else:
                    for var in variables:
                        sub_datasets = []
                        for date in dates:
                            sub_data = ds.sel(time=date)[var.key].to_numpy()
                            if sub_data.ndim == 2:
                                sub_data = np.expand_dims(sub_data, axis=0)
                            sub_datasets.append(sub_data)
                        datasets.append(np.concatenate([data for data in sub_datasets], axis=0))

        np_stack: np.ndarray = np.stack([data for data in datasets], axis=0)

        xcoords: np.ndarray = np.linspace(
            dst_bounds.left + 0.5 * resolution,
            dst_bounds.left - 0.5 * resolution + dst_sizex * resolution, dst_sizex)

        ycoords: np.ndarray = np.linspace(
            dst_bounds.top - 0.5 * resolution,
            dst_bounds.top + 0.5 * resolution - dst_sizey * resolution, dst_sizey)

        return np_stack, time, xcoords, ycoords, dst_crs

    def read_as_xarray(
        self,
        variables: Optional[List[ERA5Var]] = None,
        dates: Optional[List[str]] = None,
        crs: Optional[str] = None,
        resolution: Optional[float] = None,
        bounds: Optional[rio.coords.BoundingBox] = None,
        algorithm=rio.enums.Resampling.nearest,
        dtype: np.dtype = np.dtype('float32')) -> xr.Dataset:
        """
        Read data from ERA5 products as xarray dataset. 

        :param variables: The list of variables to read
        :param crs: Projection in which to read the data
        :param resolution: Resolution of data
        :param bounds: New bounds for datasets. 
        :param algorithm: The resampling algorithm to be used if needed
        :param dtype: dtype of the output Tensor
        :param dtype: dtype of the output Tensor
        :return:
        """
        if variables is None:
            variables = self.__available_variables

        np_arr, time, xcoords, ycoords, crs = self.read_as_numpy(variables, dates, crs, resolution,
                                                                 bounds, algorithm, dtype)

        data = {}
        if np_arr is not None:
            for i, var in enumerate(variables):
                data[var.key] = (["t", "y", "x"], np_arr[i])

        if time is not None and xcoords is not None and ycoords is not None:
            xarr = xr.Dataset(data,
                              coords={
                                  't': time,
                                  'x': xcoords,
                                  'y': ycoords
                              },
                              attrs={'crs': crs})
        else:
            xarr = None
        return xarr
