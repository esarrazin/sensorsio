#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright: (c) 2024 CESBIO / Centre National d'Etudes Spatiales
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This module contains tests for ERA5 and ERA5-land data driver
"""
import os
from dataclasses import dataclass, field
from typing import List, Optional, Tuple

import affine  # type: ignore
import numpy as np
import pytest
import rasterio as rio
from pyproj import CRS

from sensorsio import era5


def get_era5_product() -> str:
    """
    Retrieve ERA5 product path
    """
    return os.path.join(os.environ['SENSORSIO_TEST_DATA_PATH'], 'era5', "download_era5.nc")


def get_era5land_product() -> str:
    """
    Retrieve ERA5-Land product path
    """
    return os.path.join(os.environ['SENSORSIO_TEST_DATA_PATH'], 'era5', "download_era5land.zip")


def test_era5var_instantiate():
    """
    Test ERA5Var
    """
    var = era5.ERA5Var.from_key("ssrd")
    assert var.key == "ssrd"
    var = era5.ERA5Var("ssrd")
    assert var.key == "ssrd"
    with pytest.raises(ValueError):
        var = era5.ERA5Var("toto")


@pytest.mark.requires_test_data
def test_era5land_instantiate():
    """
    Test ERA5Data class instantiation 
    """
    era5_ds = era5.ERA5Data(get_era5land_product())

    assert era5_ds.filename == get_era5land_product()
    assert era5_ds.get_available_dates() == [
        '2023-03-01 00:00:00', '2023-03-01 01:00:00', '2023-03-01 02:00:00', '2023-03-01 03:00:00',
        '2023-03-01 04:00:00', '2023-03-01 05:00:00', '2023-03-01 06:00:00', '2023-03-01 07:00:00',
        '2023-03-01 08:00:00', '2023-03-01 09:00:00', '2023-03-01 10:00:00', '2023-03-01 11:00:00',
        '2023-03-01 12:00:00', '2023-03-01 13:00:00', '2023-03-01 14:00:00', '2023-03-01 15:00:00',
        '2023-03-01 16:00:00', '2023-03-01 17:00:00', '2023-03-01 18:00:00', '2023-03-01 19:00:00',
        '2023-03-01 20:00:00', '2023-03-01 21:00:00', '2023-03-01 22:00:00', '2023-03-01 23:00:00'
    ]
    assert era5_ds.get_available_variables() == ['ssrd', 'strd']
    assert era5_ds.bounds == rio.coords.BoundingBox(left=-17.05,
                                                    bottom=15.149999,
                                                    right=-14.95,
                                                    top=16.75)
    assert era5_ds.transform == affine.Affine(0.1, 0.0, -17.05, 0.0, -0.1, 16.75)
    assert era5_ds.crs == CRS.from_epsg(4326).to_string()
    assert era5_ds.res == 0.1


@pytest.mark.requires_test_data
def test_era5_instantiate():
    """
    Test ERA5Data class instantiation 
    """
    era5_ds = era5.ERA5Data(get_era5_product())

    assert era5_ds.filename == get_era5_product()
    assert era5_ds.get_available_dates() == [
        '2023-03-01 00:00:00', '2023-03-01 01:00:00', '2023-03-01 02:00:00', '2023-03-01 03:00:00',
        '2023-03-01 04:00:00', '2023-03-01 05:00:00', '2023-03-01 06:00:00', '2023-03-01 07:00:00',
        '2023-03-01 08:00:00', '2023-03-01 09:00:00', '2023-03-01 10:00:00', '2023-03-01 11:00:00',
        '2023-03-01 12:00:00', '2023-03-01 13:00:00', '2023-03-01 14:00:00', '2023-03-01 15:00:00',
        '2023-03-01 16:00:00', '2023-03-01 17:00:00', '2023-03-01 18:00:00', '2023-03-01 19:00:00',
        '2023-03-01 20:00:00', '2023-03-01 21:00:00', '2023-03-01 22:00:00', '2023-03-01 23:00:00',
        '2023-03-02 00:00:00', '2023-03-02 01:00:00', '2023-03-02 02:00:00', '2023-03-02 03:00:00',
        '2023-03-02 04:00:00', '2023-03-02 05:00:00', '2023-03-02 06:00:00', '2023-03-02 07:00:00',
        '2023-03-02 08:00:00', '2023-03-02 09:00:00', '2023-03-02 10:00:00', '2023-03-02 11:00:00',
        '2023-03-02 12:00:00', '2023-03-02 13:00:00', '2023-03-02 14:00:00', '2023-03-02 15:00:00',
        '2023-03-02 16:00:00', '2023-03-02 17:00:00', '2023-03-02 18:00:00', '2023-03-02 19:00:00',
        '2023-03-02 20:00:00', '2023-03-02 21:00:00', '2023-03-02 22:00:00', '2023-03-02 23:00:00',
        '2023-03-30 00:00:00', '2023-03-30 01:00:00', '2023-03-30 02:00:00', '2023-03-30 03:00:00',
        '2023-03-30 04:00:00', '2023-03-30 05:00:00', '2023-03-30 06:00:00', '2023-03-30 07:00:00',
        '2023-03-30 08:00:00', '2023-03-30 09:00:00', '2023-03-30 10:00:00', '2023-03-30 11:00:00',
        '2023-03-30 12:00:00', '2023-03-30 13:00:00', '2023-03-30 14:00:00', '2023-03-30 15:00:00',
        '2023-03-30 16:00:00', '2023-03-30 17:00:00', '2023-03-30 18:00:00', '2023-03-30 19:00:00',
        '2023-03-30 20:00:00', '2023-03-30 21:00:00', '2023-03-30 22:00:00', '2023-03-30 23:00:00',
        '2023-03-31 00:00:00', '2023-03-31 01:00:00', '2023-03-31 02:00:00', '2023-03-31 03:00:00',
        '2023-03-31 04:00:00', '2023-03-31 05:00:00', '2023-03-31 06:00:00', '2023-03-31 07:00:00',
        '2023-03-31 08:00:00', '2023-03-31 09:00:00', '2023-03-31 10:00:00', '2023-03-31 11:00:00',
        '2023-03-31 12:00:00', '2023-03-31 13:00:00', '2023-03-31 14:00:00', '2023-03-31 15:00:00',
        '2023-03-31 16:00:00', '2023-03-31 17:00:00', '2023-03-31 18:00:00', '2023-03-31 19:00:00',
        '2023-03-31 20:00:00', '2023-03-31 21:00:00', '2023-03-31 22:00:00', '2023-03-31 23:00:00',
        '2023-04-01 00:00:00', '2023-04-01 01:00:00', '2023-04-01 02:00:00', '2023-04-01 03:00:00',
        '2023-04-01 04:00:00', '2023-04-01 05:00:00', '2023-04-01 06:00:00', '2023-04-01 07:00:00',
        '2023-04-01 08:00:00', '2023-04-01 09:00:00', '2023-04-01 10:00:00', '2023-04-01 11:00:00',
        '2023-04-01 12:00:00', '2023-04-01 13:00:00', '2023-04-01 14:00:00', '2023-04-01 15:00:00',
        '2023-04-01 16:00:00', '2023-04-01 17:00:00', '2023-04-01 18:00:00', '2023-04-01 19:00:00',
        '2023-04-01 20:00:00', '2023-04-01 21:00:00', '2023-04-01 22:00:00', '2023-04-01 23:00:00',
        '2023-04-02 00:00:00', '2023-04-02 01:00:00', '2023-04-02 02:00:00', '2023-04-02 03:00:00',
        '2023-04-02 04:00:00', '2023-04-02 05:00:00', '2023-04-02 06:00:00', '2023-04-02 07:00:00',
        '2023-04-02 08:00:00', '2023-04-02 09:00:00', '2023-04-02 10:00:00', '2023-04-02 11:00:00',
        '2023-04-02 12:00:00', '2023-04-02 13:00:00', '2023-04-02 14:00:00', '2023-04-02 15:00:00',
        '2023-04-02 16:00:00', '2023-04-02 17:00:00', '2023-04-02 18:00:00', '2023-04-02 19:00:00',
        '2023-04-02 20:00:00', '2023-04-02 21:00:00', '2023-04-02 22:00:00', '2023-04-02 23:00:00',
        '2023-04-30 00:00:00', '2023-04-30 01:00:00', '2023-04-30 02:00:00', '2023-04-30 03:00:00',
        '2023-04-30 04:00:00', '2023-04-30 05:00:00', '2023-04-30 06:00:00', '2023-04-30 07:00:00',
        '2023-04-30 08:00:00', '2023-04-30 09:00:00', '2023-04-30 10:00:00', '2023-04-30 11:00:00',
        '2023-04-30 12:00:00', '2023-04-30 13:00:00', '2023-04-30 14:00:00', '2023-04-30 15:00:00',
        '2023-04-30 16:00:00', '2023-04-30 17:00:00', '2023-04-30 18:00:00', '2023-04-30 19:00:00',
        '2023-04-30 20:00:00', '2023-04-30 21:00:00', '2023-04-30 22:00:00', '2023-04-30 23:00:00'
    ]
    assert era5_ds.get_available_variables() == [
        'u10', 'v10', 'd2m', 't2m', 'sp', 'ssrdc', 'ssrd', 'strdc', 'strd', 'tco3', 'tcw', 'tp'
    ]
    assert era5_ds.bounds == rio.coords.BoundingBox(left=-17.125,
                                                    bottom=15.074999,
                                                    right=-14.875,
                                                    top=16.825)
    assert era5_ds.transform == affine.Affine(0.25, 0.0, -17.125, 0.0, -0.25, 16.825)
    assert era5_ds.crs == CRS.from_epsg(4326).to_string()
    assert era5_ds.res == 0.25


@dataclass(frozen=True)
class ERA5ReadAsNumpyParams:
    """
    Class to store read_as_numpy parameters
    """
    variables: List[era5.ERA5Var] = field(default_factory=lambda: [
        era5.ERA5Var.from_key('u10'),
        era5.ERA5Var.from_key('v10'),
        era5.ERA5Var.from_key('d2m'),
        era5.ERA5Var.from_key('t2m'),
        era5.ERA5Var.from_key('sp'),
        era5.ERA5Var.from_key('ssrdc'),
        era5.ERA5Var.from_key('ssrd'),
        era5.ERA5Var.from_key('strdc'),
        era5.ERA5Var.from_key('strd'),
        era5.ERA5Var.from_key('tco3')
    ])
    dates: List[str] = field(default_factory=lambda: ["2023-03-01"])
    crs: Optional[str] = None
    resolution: float = 0.25
    bounds: rio.coords.BoundingBox = rio.coords.BoundingBox(left=-17.125,
                                                            bottom=15.075,
                                                            right=-14.875,
                                                            top=16.825)
    algorithm: rio.enums.Resampling = rio.enums.Resampling.nearest
    dtype: np.dtype = np.dtype('float32')

    def expected_shape(self) -> Tuple[int, int]:
        """
        return expected shape
        """
        if self.bounds is not None:
            return (int(np.round((self.bounds[3] - self.bounds[1]) / self.resolution)),
                    int(np.round((self.bounds[2] - self.bounds[0]) / self.resolution)))

        raise NotImplementedError


@pytest.mark.parametrize(
    "parameters",
    [
        # Default
        ERA5ReadAsNumpyParams(),
        # Set variables
        ERA5ReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")]),
        # Set dates
        ERA5ReadAsNumpyParams(dates=["2023-03-01", "2023-03-01T13:00:00", "2023-03-01T18:00:00"]),
        # Set projection and resolution
        ERA5ReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")],
                              dates=["2023-03-01T10:00:00", "2023-03-01T18:00:00"],
                              crs=CRS.from_epsg(32628).to_string(),
                              resolution=1000,
                              bounds=rio.coords.BoundingBox(273000, 1669000, 513000, 1859000)),
        # Set a bounding bounding
        ERA5ReadAsNumpyParams(bounds=rio.coords.BoundingBox(-16.125, 15.5, -15.125, 16)),
        # Set a different target crs
        ERA5ReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")],
                              dates=["2023-03-01T10:00:00", "2023-03-01T18:00:00"],
                              crs=CRS.from_epsg(32628).to_string(),
                              resolution=1000,
                              bounds=rio.coords.BoundingBox(273000, 1669000, 513000, 1859000)),
    ])
def test_era5_read_as_numpy_and_xarray(parameters: ERA5ReadAsNumpyParams):
    """
    Test the read_as_numpy and read_as_numpy methods for ERA5
    """

    era5_ds = era5.ERA5Data(get_era5_product())

    # Read as numpy part
    arr, time, xcoords, ycoords, crs = era5_ds.read_as_numpy(**parameters.__dict__)

    assert arr.shape == (len(parameters.variables), len(time), *parameters.expected_shape())
    assert (~np.isnan(arr)).sum() > 0

    assert ycoords.shape == (parameters.expected_shape()[0], )
    assert xcoords.shape == (parameters.expected_shape()[1], )

    if parameters.crs is not None:
        assert crs == parameters.crs

    # Test read as xarray part
    era5_xr = era5_ds.read_as_xarray(**parameters.__dict__)

    for c in ['t', 'x', 'y']:
        assert c in era5_xr.coords

    assert era5_xr['x'].shape == (parameters.expected_shape()[1], )
    assert era5_xr['y'].shape == (parameters.expected_shape()[0], )

    if parameters.variables is not None:
        for var in parameters.variables:
            assert var.key in era5_xr.variables
            assert era5_xr[var.key].shape == (len(time), *parameters.expected_shape())

    if parameters.crs is not None:
        assert era5_xr.attrs['crs'] == parameters.crs


@dataclass(frozen=True)
class ERA5LandReadAsNumpyParams:
    """
    Class to store read_as_numpy parameters
    """
    variables: List[era5.ERA5Var] = field(
        default_factory=lambda: [era5.ERA5Var.from_key('ssrd'),
                                 era5.ERA5Var.from_key('strd')])
    dates: List[str] = field(default_factory=lambda: ["2023-03-01"])
    crs: Optional[str] = None
    resolution: float = 0.1
    bounds: rio.coords.BoundingBox = rio.coords.BoundingBox(left=-17.05,
                                                            bottom=15.15,
                                                            right=-14.95,
                                                            top=16.75)
    algorithm: rio.enums.Resampling = rio.enums.Resampling.nearest
    dtype: np.dtype = np.dtype('float32')

    def expected_shape(self) -> Tuple[int, int]:
        """
        return expected shape
        """
        if self.bounds is not None:
            return (int(np.round((self.bounds[3] - self.bounds[1]) / self.resolution)),
                    int(np.round((self.bounds[2] - self.bounds[0]) / self.resolution)))

        raise NotImplementedError


@pytest.mark.parametrize(
    "parameters",
    [
        # Default
        ERA5LandReadAsNumpyParams(),
        # Set variables
        ERA5LandReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")]),
        # Set dates
        ERA5LandReadAsNumpyParams(
            dates=["2023-03-01", "2023-03-01T13:00:00", "2023-03-01T18:00:00"]),
        # Set projection and resolution
        ERA5LandReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")],
                                  dates=["2023-03-01T10:00:00", "2023-03-01T18:00:00"],
                                  crs=CRS.from_epsg(32628).to_string(),
                                  resolution=1000,
                                  bounds=rio.coords.BoundingBox(273000, 1669000, 513000, 1859000)),
        # Set a bounding bounding
        ERA5LandReadAsNumpyParams(bounds=rio.coords.BoundingBox(-16.05, 15.45, -15.45, 15.95)),
        # Set a different target crs
        ERA5LandReadAsNumpyParams(variables=[era5.ERA5Var.from_key("ssrd")],
                                  dates=["2023-03-01T10:00:00", "2023-03-01T18:00:00"],
                                  crs=CRS.from_epsg(32628).to_string(),
                                  resolution=1000,
                                  bounds=rio.coords.BoundingBox(273000, 1669000, 513000, 1859000)),
    ])
def test_eraland5_read_as_numpy_and_xarray(parameters: ERA5LandReadAsNumpyParams):
    """
    Test the read_as_numpy and read_as_xarray methods for ERA5Land
    """

    era5_ds = era5.ERA5Data(get_era5land_product())

    # Read as numpy part
    arr, time, xcoords, ycoords, crs = era5_ds.read_as_numpy(**parameters.__dict__)

    assert arr.shape == (len(parameters.variables), len(time), *parameters.expected_shape())
    assert (~np.isnan(arr)).sum() > 0

    assert ycoords.shape == (parameters.expected_shape()[0], )
    assert xcoords.shape == (parameters.expected_shape()[1], )

    if parameters.crs is not None:
        assert crs == parameters.crs

    # Test read as xarray part
    era5_xr = era5_ds.read_as_xarray(**parameters.__dict__)

    for c in ['t', 'x', 'y']:
        assert c in era5_xr.coords

    assert era5_xr['x'].shape == (parameters.expected_shape()[1], )
    assert era5_xr['y'].shape == (parameters.expected_shape()[0], )

    if parameters.variables is not None:
        for var in parameters.variables:
            assert var.key in era5_xr.variables
            assert era5_xr[var.key].shape == (len(time), *parameters.expected_shape())

    if parameters.crs is not None:
        assert era5_xr.attrs['crs'] == parameters.crs
